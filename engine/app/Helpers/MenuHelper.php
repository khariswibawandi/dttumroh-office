<?php

namespace App\Helpers;

use App\Menu;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class MenuHelper {

     public static function getMenu()
	   {
        $data = Menu::all();
        return $data;
     }
}
