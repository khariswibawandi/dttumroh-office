<?php

namespace App\Http\Controllers;
use Datatables;
use App\Hotel;
use Illuminate\Http\Request;
use Validator;
use JWTAuth;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class HotelController extends Controller
{
    private $layout;

    public function __construct()
    {
        $this->layout='hotel/';
    }

    //
    public function index(Request $request)
    {
      /*
      $client = new Client(['base_uri' =>'http://localhost/dreamtour-backoffice/hotel', 'http_errors' => false]);
      $headers = [
        'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2RyZWFtdG91ci1iYWNrb2ZmaWNlXC9hcGlcL2xvZ2luIiwiaWF0IjoxNTY3NzgzMDMxLCJleHAiOjE1Njc3ODY2MzEsIm5iZiI6MTU2Nzc4MzAzMSwianRpIjoiVllVZ29XWXVqTWhGSEZ3RSIsInN1YiI6NCwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.lv2mFnDCSXG2unX1_D_gmbRqZRxAs8oVI7ZU6q6Z3ho',
          'Accept'        => 'application/json',
      ];
      */

      $client = new Client();
      $response = $client->request('GET', 'http://localhost/dreamtour-backoffice/hotel', [
          'headers' => [
              'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2RyZWFtdG91ci1iYWNrb2ZmaWNlXC9hcGlcL2xvZ2luIiwiaWF0IjoxNTY3Nzg0MzYzLCJleHAiOjE1Njc3ODc5NjMsIm5iZiI6MTU2Nzc4NDM2MywianRpIjoiMkJHaVE3V09DVjdKZG1uNiIsInN1YiI6NCwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.yrDVJO9_Ai0jRd6o2e6Zl7-1H2pqXTDQYAHhCjGcyiM',
              'Accept' => 'application/json',
          ],
      ]);

    return view($this->layout.'.index');
    }

    public function json()
    {
        $data = Hotel::select(['id', 'hotel_locate', 'hotel_name','aktif']);

        return Datatables::of($data)
        ->addColumn('action', function ($datas) {
            return '<a href="javascript:void(0);" id="'.$datas->id.'" onclick="update_form(this);" class="btn btn-xs btn-primary">Edit</a>&nbsp;
            <a href="javascript:void(0);" id="'.$datas->id.'" onclick="delete_data(this);" class="btn btn-xs btn-danger">Hapus</a>';
        })
        ->editColumn('id', '{{$id}}')
        ->make(true);
    }

    public function addForm(Request $request)
    {
        return view($this->layout.'._form');
    }

    public function submit(Request $request)
    {
        $messages = [
          'required' => 'required',
        ];
        $validator = Validator::make($request->all(), [
          'hotel_name' => 'required',
          'hotel_locate' 	=> 'required',
        ],$messages);

        if ($validator->passes())
        {
            $hotel = new Hotel;
            $hotel->hotel_name=$request->hotel_name;
            $hotel->hotel_locate=$request->hotel_locate;
            $hotel->aktif=$request->aktif;
            if($hotel->save())
            {
                return response()->json(['status'=>'success','mess'=>'Tambah Hotel Success']);
            }
        }

        //handle validation data
    		$data_values = $validator->errors()->toArray();
    		$keys_data = array_keys($validator->errors()->toArray());

    		for($i=0;$i<count($keys_data);$i++)
    		{
    			$arr[$keys_data[$i]] = $data_values[$keys_data[$i]][0];
    		}
    		//

        	return response()->json(['error'=>$arr]);
    }

    public function updateForm(Request $request)
    {
        $data = Hotel::where('id', $request->id)->first();
        return view($this->layout.'._form_update',compact('data'));
    }

    public function update(Request $request)
    {
      $update = Hotel::where('id', $request->id)
          ->update(['hotel_name' =>$request->hotel_name,'hotel_locate'=>$request->hotel_locate,'aktif'=>$request->aktif]);
      if($update)
      {
          return response()->json(['status'=>'success','mess'=>'Update Hotel Success']);
      }
    }

    public function delete(Request $request)
    {
        $del = Hotel::where('id', $request->id)->delete();
        if($del)
        {
            return response()->json(['status'=>'success','mess'=>'Delete Hotel Success']);
        }
    }
}
