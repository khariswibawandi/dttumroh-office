<?php

namespace App\Http\Controllers;

use App\Perwakilan;
use App\Mitra;
use Illuminate\Http\Request;
use Datatables;
use Validator;
use Illuminate\Support\Facades\DB; //use for native query

class MitraController extends Controller
{
    //
    private $layout;

    public function __construct()
    {
        $this->layout='mitra/';
    }

    //
    public function index()
    {
        return view($this->layout.'.index');
    }

    public function json()
    {
        $data = DB::table('identitas_mitra')->join('identitas_perwakilan', 'identitas_mitra.identitas_perwakilan_id', '=', 'identitas_perwakilan.id')
          ->select(['identitas_mitra.id', 'identitas_mitra.nama',
          'identitas_mitra.alamat', 'identitas_mitra.tlp',
          'identitas_mitra.nama_bank', 'identitas_mitra.no_rekening','identitas_mitra.nik','identitas_perwakilan.nama as perwakilan']);

        return Datatables::of($data)
        ->addColumn('action', function ($datas) {
            return '<a href="javascript:void(0);" id="'.$datas->id.'" onclick="update_form(this);" class="btn btn-xs btn-primary">Edit</a>&nbsp;
            <a href="javascript:void(0);" id="'.$datas->id.'" onclick="delete_data(this);" class="btn btn-xs btn-danger">Hapus</a>';
        })
        ->editColumn('id', '{{$id}}')
        ->make(true);
    }

    public function addForm(Request $request)
    {
        $perwakilan = Perwakilan::all();
        return view($this->layout.'._form',compact('perwakilan'));
    }

    public function submit(Request $request)
    {
        $messages = [
          'required' => 'required',
        ];
        $validator = Validator::make($request->all(), [
          'identitas_perwakilan_id'=>'required',
          'nama' => 'required',
          'alamat' 	=> 'required',
          'tlp' 	=> 'required',
          'nama_bank' 	=> 'required',
          'no_rekening' 	=> 'required',
          'nik' 	=> 'required',
        ],$messages);

        if ($validator->passes())
        {
            $mitra = new Mitra;
            $mitra->identitas_perwakilan_id=$request->identitas_perwakilan_id;
            $mitra->nama=$request->nama;
            $mitra->alamat=$request->alamat;
            $mitra->tlp=$request->tlp;
            $mitra->nama_bank=$request->nama_bank;
            $mitra->no_rekening=$request->no_rekening;
            $mitra->nik=$request->nik;
            if($mitra->save())
            {
                return response()->json(['status'=>'success','mess'=>'Tambah Mitra Success']);
            }
        }

        //handle validation data
        $data_values = $validator->errors()->toArray();
        $keys_data = array_keys($validator->errors()->toArray());

        for($i=0;$i<count($keys_data);$i++)
        {
          $arr[$keys_data[$i]] = $data_values[$keys_data[$i]][0];
        }
        //

          return response()->json(['error'=>$arr]);
    }

    public function updateForm(Request $request)
    {
        $data = Mitra::where('id', $request->id)->first();
        $perwakilan = Perwakilan::all();
        return view($this->layout.'._form_update',compact('data','perwakilan'));
    }

    public function update(Request $request)
    {
      $update = Mitra::where('id', $request->id)
          ->update(['nama' =>$request->nama,
                  'identitas_perwakilan_id'=>$request->identitas_perwakilan_id,
                  'alamat'=>$request->alamat,
                  'tlp'=>$request->tlp,
                  'nama_bank'=>$request->nama_bank,
                  'no_rekening'=>$request->no_rekening,
                  'nik'=>$request->nik,
                  'aktif'=>$request->aktif,]);
      if($update)
      {
          return response()->json(['status'=>'success','mess'=>'Update Mitra Success']);
      }
    }

    public function delete(Request $request)
    {
        $del = Mitra::where('id', $request->id)->delete();
        if($del)
        {
            return response()->json(['status'=>'success','mess'=>'Delete Mitra Success']);
        }
    }
}
