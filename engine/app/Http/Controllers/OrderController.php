<?php

namespace App\Http\Controllers;

use App\Perwakilan;
use App\Mitra;
use Illuminate\Http\Request;
use Datatables;
use Validator;
use Illuminate\Support\Facades\DB; //use for native query

class OrderController extends Controller
{
    //
    //
    private $layout;

    public function __construct()
    {
        $this->layout='order/';
    }
    
    public function index()
    {
			return view($this->layout.'.index');    
    }
    
    public function json()
    {
        $data = DB::table('orders')
        		->join('users', 'users.id', '=', 'orders.user_id')
        		->join('orders_status', 'orders_status.id', '=', 'orders.order_status_id')
        		->join('order_produk', 'order_produk.order_id', '=', 'orders.id')
        		->join('produks', 'produks.id', '=', 'order_produk.produk_id')
          ->select(['orders.id','produks.item_name', 'users.name as user',
          'orders_status.status_name', 'produks.item_code',
          'produks.pesawat'
      		]);

        return Datatables::of($data)
        ->addColumn('action', function ($datas) {
            return '<a href="javascript:void(0);" id="'.$datas->id.'" onclick="update_form(this);" class="btn btn-xs btn-primary">Edit</a>&nbsp;
            <a href="javascript:void(0);" id="'.$datas->id.'" onclick="delete_data(this);" class="btn btn-xs btn-danger">Hapus</a>';
        })
        ->editColumn('id', '{{$id}}')
        ->make(true);
    }
}
