<?php

namespace App\Http\Controllers;

use App\Perwakilan;
use Illuminate\Http\Request;
use Datatables;
use Validator;

class PerwakilanController extends Controller
{
  private $layout;

  public function __construct()
  {
      $this->layout='perwakilan/';
  }

  //
  public function index()
  {
      return view($this->layout.'.index');
  }

  public function json()
  {
      $data = Perwakilan::select(['id', 'nama', 'alamat','tlp','nama_bank','no_rekening','nik']);

      return Datatables::of($data)
      ->addColumn('action', function ($datas) {
          return '<a href="javascript:void(0);" id="'.$datas->id.'" onclick="update_form(this);" class="btn btn-xs btn-primary">Edit</a>&nbsp;
          <a href="javascript:void(0);" id="'.$datas->id.'" onclick="delete_data(this);" class="btn btn-xs btn-danger">Hapus</a>';
      })
      ->editColumn('id', '{{$id}}')
      ->make(true);
  }

  public function addForm(Request $request)
  {
      return view($this->layout.'._form');
  }

  public function submit(Request $request)
  {
      $messages = [
        'required' => 'required',
      ];
      $validator = Validator::make($request->all(), [
        'nama' => 'required',
        'alamat' 	=> 'required',
        'tlp' 	=> 'required',
        'nama_bank' 	=> 'required',
        'no_rekening' 	=> 'required',
        'nik' 	=> 'required',
      ],$messages);

      if ($validator->passes())
      {
          $perwakilan = new Perwakilan;
          $perwakilan->identitas_pusat_id=1;
          $perwakilan->nama=$request->nama;
          $perwakilan->alamat=$request->alamat;
          $perwakilan->tlp=$request->tlp;
          $perwakilan->nama_bank=$request->nama_bank;
          $perwakilan->no_rekening=$request->no_rekening;
          $perwakilan->nik=$request->nik;
          if($perwakilan->save())
          {
              return response()->json(['status'=>'success','mess'=>'Tambah Perwakilan Success']);
          }
      }

      //handle validation data
      $data_values = $validator->errors()->toArray();
      $keys_data = array_keys($validator->errors()->toArray());

      for($i=0;$i<count($keys_data);$i++)
      {
        $arr[$keys_data[$i]] = $data_values[$keys_data[$i]][0];
      }
      //

        return response()->json(['error'=>$arr]);
  }

  public function updateForm(Request $request)
  {
      $data = Perwakilan::where('id', $request->id)->first();
      return view($this->layout.'._form_update',compact('data'));
  }

  public function update(Request $request)
  {
    $update = Perwakilan::where('id', $request->id)
        ->update(['nama' =>$request->nama,
                'alamat'=>$request->alamat,
                'tlp'=>$request->tlp,
                'nama_bank'=>$request->nama_bank,
                'no_rekening'=>$request->no_rekening,
                'nik'=>$request->nik,
                'aktif'=>$request->aktif,]);
    if($update)
    {
        return response()->json(['status'=>'success','mess'=>'Update Perwakilan Success']);
    }
  }

  public function delete(Request $request)
  {
      $del = Perwakilan::where('id', $request->id)->delete();
      if($del)
      {
          return response()->json(['status'=>'success','mess'=>'Delete Perwakilan Success']);
      }
  }
}
