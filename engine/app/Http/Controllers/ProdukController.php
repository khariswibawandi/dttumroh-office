<?php

namespace App\Http\Controllers;

use Datatables;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB; //use for native query
use App\ProdukKategori;
use App\Bandara;
use App\ProdukStok;
use App\Produk;
use Illuminate\Support\Facades\Hash;

class ProdukController extends Controller
{
    //
    //
    private $layout;

    public function __construct()
    {
        $this->layout='produk/';
    }

    //
    public function index()
    {
        return view($this->layout.'.index');
    }

    public function json()
    {
        $data = DB::table('produks')
          ->join('produk_kategori', 'produks.produk_kategori_id', '=', 'produk_kategori.id')
          ->join('bandaras', 'bandaras.id', '=', 'produks.bandara_id')
          ->select(['produks.id',
          'produks.item_code', 'produks.item_name','produks.pesawat','produk_kategori.nama as kategori','bandaras.lokasi_bandara']);

        return Datatables::of($data)
        ->addColumn('action', function ($datas) {
            return '<a href="javascript:void(0);" id="'.$datas->id.'" onclick="update_form(this);" class="btn btn-xs btn-primary">Edit</a>&nbsp;
            <a href="javascript:void(0);" id="'.$datas->id.'" onclick="delete_data(this);" class="btn btn-xs btn-danger">Hapus</a>';
        })
        ->editColumn('id', '{{$id}}')
        ->make(true);
    }

    public function addForm(Request $request)
    {
        $kategori = ProdukKategori::all();
        $bandara = Bandara::all();
        return view($this->layout.'._form',compact('kategori','bandara'));
    }

    public function submit(Request $request)
    {
        $messages = [
          'required' => 'required',
        ];

        //print "<pre>".print_r($request->all(),true)."</pre>";die;

        $validator = Validator::make($request->all(), [
          'bandara_id'=>'required',
          'produk_kategori_id' => 'required',
          'item_code' 	=> 'required',
          'item_name' 	=> 'required',
          'pesawat' 	=> 'required',
          'harga_dtt_duta' 	=> 'required',
          'harga_dtt_perwakilan' 	=> 'required',
          'harga_dtt_pusat' 	=> 'required',
          'stok' 	=> 'required',
        ],$messages);

        if ($validator->passes())
        {
            $produk = new Produk;
            $produk->bandara_id=$request->bandara_id;
            $produk->produk_kategori_id=$request->produk_kategori_id;
            $produk->item_code	=$request->item_code;
            $produk->item_name=$request->item_name;
            $produk->pesawat=$request->pesawat;
            $produk->harga_dtt_duta=$request->harga_dtt_duta;
            $produk->perelengkapan_handling=$request->perelengkapan_handling;
            $produk->harga_dtt_pusat=$request->harga_dtt_pusat;
            $produk->harga_dtt_perwakilan=$request->harga_dtt_perwakilan;
            if($produk->save())
            {
                $ps = new ProdukStok;
                $ps->produk_id = $produk->id;
                $ps->jumlah = $request->stok;
                if($ps->save())
                {
                    return response()->json(['status'=>'success','mess'=>'Tambah Produk Success']);
                }
            }
        }

        //handle validation data
        $data_values = $validator->errors()->toArray();
        $keys_data = array_keys($validator->errors()->toArray());

        for($i=0;$i<count($keys_data);$i++)
        {
          $arr[$keys_data[$i]] = $data_values[$keys_data[$i]][0];
        }
        //
        return response()->json(['error'=>$arr]);
    }

    public function updateForm(Request $request)
    {
        $data = Produk::where('id', $request->id)->first();
        $kategori = ProdukKategori::all();
        $bandara = Bandara::all();
        $stok = ProdukStok::where('produk_id',$request->id)->first();
        return view($this->layout.'._form_update',compact('data','kategori','bandara','stok'));
    }

    public function update(Request $request)
    {
      $update = Produk::where('id', $request->id)
          ->update(['bandara_id' =>$request->bandara_id,
                  'produk_kategori_id'=>$request->produk_kategori_id,
                  'item_code'=>$request->item_code,
                  'item_name'=>$request->item_name,
                  'pesawat'=>$request->pesawat,
                  'harga_dtt_duta'=>$request->harga_dtt_duta,
                  'harga_dtt_pusat'=>$request->harga_dtt_pusat,
                  'harga_dtt_perwakilan'=>$request->harga_dtt_perwakilan,]);
      if($update)
      {
          ProdukStok::where('produk_id',$request->id)
          ->update(
            [
              'jumlah'=>$request->stok
            ]
          );
          return response()->json(['status'=>'success','mess'=>'Update Produk Success']);
      }
    }

    public function delete(Request $request)
    {
        $del = Produk::where('id', $request->id)->delete();
        if($del)
        {
            $del = ProdukStok::where('produk_id',$request->id)->delete();
            if($del)
            {
              return response()->json(['status'=>'success','mess'=>'Delete Produk Success']);
            }
        }
    }
}
