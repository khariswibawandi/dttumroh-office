<form class="form" id="myForm">
  <input type="hidden" name="id" value="<?php echo $data->id; ?>">
  <div class="form-group mt-5 row">
      <label for="example-text-input" class="col-2 col-form-label">Nama Hotel</label>
      <div class="col-10">
          <input class="form-control" type="text" id="hotel_name" name="hotel_name" value="<?php echo $data->hotel_name ?>">
      </div>
  </div>
  <div class="form-group row">
      <label for="example-search-input" class="col-2 col-form-label">Lokasi</label>
      <div class="col-10">
          <input class="form-control" type="search" id="hotel_locate" name="hotel_locate" value="<?php echo $data->hotel_locate ?>">
      </div>
  </div>
  <div class="form-group row">
      <label for="example-email-input" class="col-2 col-form-label">Aktif</label>
      <div class="col-10">
          <select name="aktif" class="form-control">
            <?php
                if($data->aktif=='y')
                {
                ?>
                  <option selected="selected" value='y'>Yes</option>
                  <option value='n'>No</option>
                <?php
                }
                else
                {
                ?>
                <option value='y'>Yes</option>
                <option selected="selected"  value='n'>No</option>
                <?php
                }
             ?>

          </select>
      </div>
  </div>
    <button class="btn btn-success" id="btn-submit">Update</button>
</form>
<script>
$('#btn-submit').on('click', function (e) {
		e.preventDefault();
			var data = $('#myForm').serialize();
			$.ajax({
				url:"{{ url('/api/hotel/update') }}", // point to server-side PHP script
				//dataType: 'json', // what to expect back from the PHP script
				data: data,
				beforeSend:function(){
					//$.LoadingOverlay("show");
				},
				type: 'post',
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				success: function (row) {
					//$.LoadingOverlay("hide", true);
          if(row.status=='success')
          {
              dt_table.ajax.reload(null,false);
              $('#myModal').modal('toggle');
          }
				},
				error: function (response) {
					 $('#loading-bar').hide();
				}
			});
			return false;

	});
</script>
