<form class="form" id="myForm">
    <input type="hidden" name="id" value="<?php echo $data->id ?>">
    <div class="form-group row">
        <label for="example-text-input" class="col-2 col-form-label">Nama</label>
        <div class="col-6">
            <input class="form-control" type="text" id="nama" name="nama" value="<?php echo $data->nama ?>">
        </div>
        <div class="col-2">
            <span id="Error-nama"></span>
        </div>
    </div>
    <div class="form-group row">
        <label for="example-text-input" class="col-2 col-form-label">NIK</label>
        <div class="col-6">
            <input class="form-control" type="text" id="nik" name="nik" value="<?php echo $data->nik ?>">
        </div>
        <div class="col-2">
            <span id="Error-nik"></span>
        </div>
    </div>
    <div class="form-group row">
        <label for="example-search-input" class="col-2 col-form-label">Alamat</label>
        <div class="col-6">
            <textarea id="alamat" name="alamat" class="form-control" rows="5" cols="10"><?php echo $data->alamat ?></textarea>
        </div>
        <div class="col-2">
            <span id="Error-alamat"></span>
        </div>
    </div>
    <div class="form-group row">
        <label for="example-text-input" class="col-2 col-form-label">Telepon</label>
        <div class="col-4">
            <input class="form-control" type="text" id="tlp" name="tlp" value="<?php echo $data->tlp ?>">
        </div>
        <div class="col-2">
            <span id="Error-tlp"></span>
        </div>
    </div>
    <div class="form-group row">
        <label for="example-text-input" class="col-2 col-form-label">Bank</label>
        <div class="col-6">
            <input class="form-control" type="text" id="nama_bank" name="nama_bank" value="<?php echo $data->nama_bank ?>">
        </div>
        <div class="col-2">
            <span id="Error-nama_bank"></span>
        </div>
    </div>
    <div class="form-group row">
        <label for="example-text-input" class="col-2 col-form-label">No. Rek</label>
        <div class="col-6">
            <input class="form-control" type="text" id="no_rekening" name="no_rekening" value="<?php echo $data->no_rekening ?>">
        </div>
        <div class="col-2">
            <span id="Error-no_rekening"></span>
        </div>
    </div>
    <div class="form-group row">
        <label for="example-email-input" class="col-2 col-form-label">Aktif</label>
        <div class="col-2">
            <select name="aktif" class="form-control">
              <?php
                  if($data->aktif=='y')
                  {
                  ?>
                    <option selected="selected" value='y'>Yes</option>
                    <option value='n'>No</option>
                  <?php
                  }
                  else
                  {
                  ?>
                  <option value='y'>Yes</option>
                  <option selected="selected"  value='n'>No</option>
                  <?php
                  }
               ?>

            </select>
        </div>
    </div>
    <button class="btn btn-success" id="btn-submit">Update</button>
</form>
<script>
$('#btn-submit').on('click', function (e) {
		e.preventDefault();
			var data = $('#myForm').serialize();
			$.ajax({
				url:"{{ url('/api/perwakilan/update') }}", // point to server-side PHP script
				//dataType: 'json', // what to expect back from the PHP script
				data: data,
				beforeSend:function(){
					//$.LoadingOverlay("show");
				},
				type: 'post',
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				success: function (row) {
					//$.LoadingOverlay("hide", true);
          if(row.status=='success')
          {
              dt_table.ajax.reload(null,false);
              $('#myModal').modal('toggle');
          }
				},
				error: function (response) {
					 $('#loading-bar').hide();
				}
			});
			return false;

	});
</script>
