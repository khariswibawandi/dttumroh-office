@extends('layouts.app')
@section('content')
<link href="{{asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
<a href="javascript:void(0);" onclick="add_form();" class="btn btn-primary">Tambah</a>
<div class="table-responsive">
    <table id="dt-data" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nama</th>
                <th>NIK</th>
                <th>Alamat</th>
                <th>Tlp</th>
                <th>Bank</th>
                <th>No. Rek</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
<script src="{{ asset('assets/libs/jquery/dist/jquery.min.js')}}"></script>
<script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script>
var dt_table = $('#dt-data').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ url('api/perwakilan/json') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'nama', name: 'hotel_name'},
            {data: 'nik', name: 'hotel_locate'},
            {data: 'alamat', name: 'hotel_locate'},
            {data: 'tlp', name: 'hotel_locate'},
            {data: 'nama_bank', name: 'hotel_locate'},
            {data: 'no_rekening', name: 'no_rekening'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });

function add_form()
{
    $('.modal-title').html("");
    $('.modal-title').html("Tambah Perwakilan");

    $.ajax({
      url:"{{ url('/api/perwakilan/addform') }}", // point to server-side PHP script
      //dataType: 'json', // what to expect back from the PHP script
      beforeSend:function(){
        //$.LoadingOverlay("show");
      },
      type: 'post',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function (data) {
        //$.LoadingOverlay("hide", true);
        $('#myModal').modal('show', {backdrop: 'static', keyboard: false});
        $('#myModal .modal-body').html(data);
      },
      error: function (response) {
         $('#loading-bar').hide();
      }
    });
}

function update_form(p)
{
    console.log(p.id);
    var tmp = p.id;
    $('.modal-title').html("");
    $('.modal-title').html("Ubah Hotel");

    var data = 'id='+tmp;
    $.ajax({
      url:"{{ url('/api/perwakilan/updateform') }}", // point to server-side PHP script
      //dataType: 'json', // what to expect back from the PHP script
      beforeSend:function(){
        //$.LoadingOverlay("show");
      },
      data:data,
      type: 'post',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function (data) {
        //$.LoadingOverlay("hide", true);
        $('#myModal').modal('show', {backdrop: 'static', keyboard: false});
        $('#myModal .modal-body').html(data);
      },
      error: function (response) {
         $('#loading-bar').hide();
      }
    });
}

function delete_data(p)
{
    console.log(p.id);
    var tmp = p.id;
    var data = 'id='+tmp;
    if(confirm('apakah anda yakin delete data?'))
    {
        $.ajax({
          url:"{{ url('/api/perwakilan/delete') }}", // point to server-side PHP script
          //dataType: 'json', // what to expect back from the PHP script
          beforeSend:function(){
            //$.LoadingOverlay("show");
          },
          data:data,
          type: 'post',
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          success: function (data) {
            //$.LoadingOverlay("hide", true);
            dt_table.ajax.reload(null,false);
          },
          error: function (response) {
             $('#loading-bar').hide();
          }
        });
    }
}
</script>
<div id="myModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myLargeModalLabel"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                          </div>
            <div class="modal-footer">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection
