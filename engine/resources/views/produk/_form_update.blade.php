
<ul class="nav nav-tabs" role="tablist">
    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Produk</span></a> </li>
    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Harga</span></a> </li>
    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#messages" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Stok</span></a> </li>
</ul>
<!-- Tab panes -->
<div class="tab-content tabcontent-border">
    <div class="tab-pane active" id="home" role="tabpanel">
      <br>
      <form class="form" id="form1">
        <input type="hidden" name="id" value="<?php echo $data->id ?>">
        <div class="form-group row">
            <label for="example-email-input" class="col-2 col-form-label">Bandara</label>
            <div class="col-6">
                <select name="bandara_id" class="form-control" id="bandara_id">
                    <option value="">-Pilih Bandara-</option>
                    <?php
                      foreach($bandara as $val)
                      {
                        if($data->bandara_id==$val->id)
                        {
                        ?>
                          <option selected="selected" value="<?php echo $val->id ?>"><?php echo $val->lokasi_bandara ?></option>
                        <?php
                        }
                        else
                        {
                        ?>
                          <option value="<?php echo $val->id ?>"><?php echo $val->lokasi_bandara ?></option>
                        <?php
                        }
                      }
                    ?>
                </select>
            </div>
            <div class="col-2">
                <span id="Error-bandara_id"></span>
            </div>
        </div>
          <div class="form-group row">
              <label for="example-text-input" class="col-2 col-form-label">Kategori</label>
              <div class="col-6">
                <select name="produk_kategori_id" id="produk_kategori_id" class="form-control">
                    <option value="">-Pilih Kategori-</option>
                    <?php
                      foreach($kategori as $val)
                      {
                        if($val->id==$data->produk_kategori_id)
                        {
                        ?>
                          <option  selected="selected" value="<?php echo $val->id ?>"><?php echo $val->nama ?></option>
                        <?php
                        }
                        else
                        {
                        ?>
                          <option value="<?php echo $val->id ?>"><?php echo $val->nama ?></option>
                        <?php
                        }
                      }
                    ?>
                </select>
              </div>
              <div class="col-2">
                  <span id="Error-produk_kategori_id"></span>
              </div>
          </div>
          <div class="form-group row">
              <label for="example-text-input" class="col-2 col-form-label">Kode</label>
              <div class="col-6">
                  <input class="form-control" type="text" id="item_code" name="item_code" value="<?php echo $data->item_code ?>">
              </div>
              <div class="col-2">
                  <span id="Error-item_code"></span>
              </div>
          </div>
          <div class="form-group row">
              <label for="example-search-input" class="col-2 col-form-label">Nama</label>
              <div class="col-6">
                  <input class="form-control" type="text" id="item_name" name="item_name" value="<?php echo $data->item_code ?>">
              </div>
              <div class="col-2">
                  <span id="Error-item_name"></span>
              </div>
          </div>
          <div class="form-group row">
              <label for="example-text-input" class="col-2 col-form-label">Pesawat</label>
              <div class="col-6">
                  <input class="form-control" type="text" id="pesawat" name="pesawat" value="<?php echo $data->pesawat ?>">
              </div>
              <div class="col-2">
                  <span id="Error-pesawat"></span>
              </div>
          </div>
          <div class="form-group row">
              <label for="example-text-input" class="col-2 col-form-label">Perlenkapan Handling</label>
              <div class="col-6">
                  <input class="form-control" type="text" id="perelengkapan_handling" name="perelengkapan_handling" value="<?php echo $data->perelengkapan_handling ?>">
              </div>
              <div class="col-2">
                  <span id="Error-perelengkapan_handling"></span>
              </div>
          </div>
      </form>
    </div>
    <div class="tab-pane  p-20" id="profile" role="tabpanel">
      <form class="form" id="form2">
          <div class="form-group row">
              <label for="example-text-input" class="col-2 col-form-label">Harga DTT Duta</label>
              <div class="col-6">
                  <input class="form-control" type="text" id="harga_dtt_duta" name="harga_dtt_duta" value="<?php echo $data->harga_dtt_duta ?>">
              </div>
              <div class="col-2">
                  <span id="Error-harga_dtt_duta"></span>
              </div>
          </div>
          <div class="form-group row">
              <label for="example-search-input" class="col-2 col-form-label">Harga DTT Pusat</label>
              <div class="col-6">
                  <input class="form-control" type="text" id="harga_dtt_pusat" name="harga_dtt_pusat" value="<?php echo $data->harga_dtt_pusat ?>">
              </div>
              <div class="col-2">
                  <span id="Error-harga_dtt_pusat"></span>
              </div>
          </div>
          <div class="form-group row">
              <label for="example-text-input" class="col-2 col-form-label">Harga DTT Perwakilan</label>
              <div class="col-4">
                  <input class="form-control" type="text" id="harga_dtt_perwakilan" name="harga_dtt_perwakilan" value="<?php echo $data->harga_dtt_perwakilan ?>">
              </div>
              <div class="col-2">
                  <span id="Error-harga_dtt_perwakilan"></span>
              </div>
          </div>
      </form>
    </div>
    <div class="tab-pane p-20" id="messages" role="tabpanel">
      <form class="form" id="form3">
          <div class="form-group row">
              <label for="example-text-input" class="col-2 col-form-label">Stok</label>
              <div class="col-6">
                  <input class="form-control" type="text" id="stok" name="stok" value="<?php echo $stok->jumlah ?>">
              </div>
              <div class="col-2">
                  <span id="Error-stok"></span>
              </div>
          </div>
      </form>
    </div>
    <button class="btn btn-success" id="btn-submit">Update</button>
</div>

<script>
$('#btn-submit').on('click', function (e) {
		e.preventDefault();
			var data = $('#form1,#form2,#form3').serialize();
			$.ajax({
				url:"{{ url('/api/produk/update') }}", // point to server-side PHP script
				//dataType: 'json', // what to expect back from the PHP script
				data: data,
				beforeSend:function(){
					//$.LoadingOverlay("show");
				},
				type: 'post',
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				success: function (row) {
					//$.LoadingOverlay("hide", true);
          if(row.status=='success')
          {
              dt_table.ajax.reload(null,false);
              $('#myModal').modal('toggle');
          }
          else
          {
              printErrorMsg(row.error);
          }
				},
				error: function (response) {
					 $('#loading-bar').hide();
				}
			});
			return false;
	});

  function printErrorMsg (msg) {
		$.each( msg, function( key, value ) {
			$("#Error-"+key).html('');
			$("#Error-"+key).html(value);
			$("#Error-"+key).show();
			$("#Error-"+key).css({'color':'red'});
		});
	}
</script>
