<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'UserController@login');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('hotel/json','HotelController@json');
Route::post('hotel/addform','HotelController@addForm');
Route::post('hotel/submit','HotelController@submit');
Route::post('hotel/updateform','HotelController@updateForm');
Route::post('hotel/update','HotelController@update');
Route::post('hotel/delete','HotelController@delete');

Route::get('perwakilan/json','PerwakilanController@json');
Route::post('perwakilan/addform','PerwakilanController@addForm');
Route::post('perwakilan/submit','PerwakilanController@submit');
Route::post('perwakilan/updateform','PerwakilanController@updateForm');
Route::post('perwakilan/update','PerwakilanController@update');
Route::post('perwakilan/delete','PerwakilanController@delete');

Route::get('mitra/json','MitraController@json');
Route::post('mitra/addform','MitraController@addForm');
Route::post('mitra/submit','MitraController@submit');
Route::post('mitra/updateform','MitraController@updateForm');
Route::post('mitra/update','MitraController@update');
Route::post('mitra/delete','MitraController@delete');

Route::get('produk/json','ProdukController@json');
Route::post('produk/addform','ProdukController@addForm');
Route::post('produk/submit','ProdukController@submit');
Route::post('produk/updateform','ProdukController@updateForm');
Route::post('produk/update','ProdukController@update');
Route::post('produk/delete','ProdukController@delete');

Route::get('orders/json','OrderController@json');
Route::post('orders/addform','OrderController@addForm');
Route::post('orders/submit','OrderController@submit');
Route::post('orders/updateform','OrderController@updateForm');
Route::post('orders/update','OrderController@update');
Route::post('orders/delete','OrderController@delete');
