<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Auth\LoginController@index');

//Route::middleware('jwt.verify')->group(function(){
    Route::get('hotel','HotelController@index');
//});

Route::get('perwakilan','PerwakilanController@index');
Route::get('mitra','MitraController@index');
Route::get('produk','ProdukController@index');
Route::get('orders','OrderController@index');
